package com.example.weatertop;

import com.example.weatertop.controller.WeatherController;
import com.example.weatertop.model.Weather;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class WeatherControllerTest {
    @Test
    public void getWeatherByAPI(){

    }
    WeatherController weatherControllerMock = mock(WeatherController.class);

    @Test
    public void tsetGetWeatherByDB(){
        String moscowCity = "moscow";
        String permCity = "perm";

        int moscowTemp = 12;
        int permTemp = 5;

        when(weatherControllerMock.getWeatherByDB(moscowCity)).thenReturn(ResponseEntity.ok(moscowTemp));
        when(weatherControllerMock.getWeatherByDB(permCity)).thenReturn(ResponseEntity.ok(permTemp));

        ResponseEntity<Integer> responseMoscow = weatherControllerMock.getWeatherByDB(moscowCity);
        ResponseEntity<Integer> responsePerm = weatherControllerMock.getWeatherByDB(permCity);

        assertEquals(ResponseEntity.ok(moscowTemp), responseMoscow);
        assertEquals(ResponseEntity.ok(permTemp), responsePerm);

        System.out.println("Метод getWeatherByDB протестирован.");
    }

    @Test
    public void testGetWeatherFromDB(){
        String moscowCity = "moscow";
        String mexicoCity = "mexico";

        String moscowJson = "{\"id\":1," +
                "\"city\":\"moscow\"," +
                "\"cloud_pct\":4," +
                "\"temp\":12," +
                "\"feels_like\":11," +
                "\"humidity\":55," +
                "\"min_temp\":11," +
                "\"max_temp\":14," +
                "\"wind_speed\":4.26," +
                "\"wind_degrees\":71.0," +
                "\"sunrise\":1686530737," +
                "\"sunset\":1686593588}";

        String mexicoJson = "{\"id\":7," +
                "\"city\":\"mexico\"," +
                "\"cloud_pct\":81," +
                "\"temp\":31," +
                "\"feels_like\":35," +
                "\"humidity\":63," +
                "\"min_temp\":31," +
                "\"max_temp\":31," +
                "\"wind_speed\":6.03," +
                "\"wind_degrees\":239.0," +
                "\"sunrise\":1686605213," +
                "\"sunset\":1686652041}";

        when(weatherControllerMock.getWeatherFromDB(moscowCity)).thenReturn(ResponseEntity.ok(moscowJson));
        when(weatherControllerMock.getWeatherFromDB(mexicoCity)).thenReturn(ResponseEntity.ok(mexicoJson));

        ResponseEntity<String> responseMoscow = weatherControllerMock.getWeatherFromDB(moscowCity);
        ResponseEntity<String> responseMexico = weatherControllerMock.getWeatherFromDB(mexicoCity);

        assertEquals(ResponseEntity.ok(moscowJson), responseMoscow);
        assertEquals(ResponseEntity.ok(mexicoJson), responseMexico);

        System.out.println("Метод getWeatherFromDB протестирован.");
    }

    @Test
    public void testGetWeatherByAPI() throws Exception{
        String voronezhCity = "voronezh";
        String penzaCity = "penza";

        int voronezTemp = 14;
        int penzaTemp = 13;

        when(weatherControllerMock.getWeatherByAPI(voronezhCity)).thenReturn(ResponseEntity.ok(voronezTemp));
        when(weatherControllerMock.getWeatherByAPI(penzaCity)).thenReturn(ResponseEntity.ok(penzaTemp));

        ResponseEntity<Integer> responeVoronezh = weatherControllerMock.getWeatherByAPI(voronezhCity);
        ResponseEntity<Integer> responePenza = weatherControllerMock.getWeatherByAPI(penzaCity);

        assertEquals(ResponseEntity.ok(voronezTemp), responeVoronezh);
        assertEquals(ResponseEntity.ok(penzaTemp), responePenza);

        System.out.println("Метод getWeatherByAPI протестирован.");
    }
}
