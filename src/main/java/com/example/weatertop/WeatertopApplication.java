package com.example.weatertop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatertopApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatertopApplication.class, args);
	}

}
