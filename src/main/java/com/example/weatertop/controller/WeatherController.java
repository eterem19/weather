package com.example.weatertop.controller;

import com.example.weatertop.jms.WeatherProducer;
import com.example.weatertop.model.Weather;
import com.example.weatertop.service.impl.WeatherServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
@RequestMapping("/weather")
@EnableJms
public class WeatherController {

    private final WeatherProducer weatherProducer;
    private final WeatherServiceImpl weatherServiceImpl;
    @Autowired
    public WeatherController(WeatherProducer weatherProducer, WeatherServiceImpl weatherServiceImpl) {
        this.weatherProducer = weatherProducer;
        this.weatherServiceImpl = weatherServiceImpl;
    }

    @GetMapping("/api/{city}")
    public ResponseEntity<Integer> getWeatherByAPI(@PathVariable String city) throws Exception{
        URL url = new URL("https://api.api-ninjas.com/v1/weather?city=" + city);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("X-Api-Key", "aCQf5mYIfKNIhy2etdORbQ==4lC2SUpMjyg9CTpq");
        InputStream responseStream = connection.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(responseStream);
        Weather weather = mapper.readValue(root.toString(), Weather.class);
        weather.setCity(city);

        weatherProducer.sendWeather(weather);

        int currentTemperature = weather.getTemp();
        return ResponseEntity.ok(currentTemperature);
    }
    @GetMapping("/db/{city}")
    public ResponseEntity<Integer> getWeatherByDB(@PathVariable String city){
        ObjectMapper mapper = new ObjectMapper();
        String json = getWeatherFromDB(city).getBody();
        int temp = 0;

        try{
            Weather weather = mapper.readValue(json, Weather.class);
            temp = weather.getTemp();
            return ResponseEntity.ok(temp);
        }catch (Exception exception){

        }
        return null;
    }
    @GetMapping("/json/{city}")
    public ResponseEntity<String> getWeatherFromDB(@PathVariable String city){
        ObjectMapper objectMapper = new ObjectMapper();

        try{
            String json = objectMapper.writeValueAsString(weatherServiceImpl.findWeatherByCity(city));
            return ResponseEntity.ok(json);
        } catch (Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
}

