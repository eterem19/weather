package com.example.weatertop.service.impl;

import com.example.weatertop.model.Weather;
import com.example.weatertop.repository.WeatherRepository;
import com.example.weatertop.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherServiceImpl implements WeatherService {

    private final WeatherRepository weatherRepository;
    @Autowired
    public WeatherServiceImpl(WeatherRepository weatherRepository){
        this.weatherRepository = weatherRepository;
    }
    @Override
    public void saveWeather(Weather weather) {
        weatherRepository.save(weather);
        System.out.println("Объект сохранён в бд.");
    }

    @Override
    public Weather findWeatherByCity(String city) {
        return weatherRepository.findWeatherByCity(city);
    }
}
