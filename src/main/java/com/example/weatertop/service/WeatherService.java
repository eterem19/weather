package com.example.weatertop.service;

import com.example.weatertop.model.Weather;

public interface WeatherService {
    void saveWeather(Weather weather);
    Weather findWeatherByCity(String city);
}
