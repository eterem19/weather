package com.example.weatertop.jms;

import com.example.weatertop.model.Weather;
import com.example.weatertop.service.impl.WeatherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class WeatherListener {

    private final WeatherServiceImpl weatherServiceImpl;

    @Autowired
    public WeatherListener(WeatherServiceImpl weatherServiceImpl){
        this.weatherServiceImpl = weatherServiceImpl;
    }
    @JmsListener(destination = "weatherQueue")
    public void receiveWeather(Weather weather) {
        weatherServiceImpl.saveWeather(weather);
    }
}
