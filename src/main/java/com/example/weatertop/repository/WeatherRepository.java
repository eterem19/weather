package com.example.weatertop.repository;

import com.example.weatertop.model.Weather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface WeatherRepository extends JpaRepository<Weather, Long> {
    Weather findWeatherByCity(String city);
}